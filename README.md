#SpringBoot-MyBatis-Redis

搭建一个简单的SpringBoot+MyBatis+Redis+MyBatisPlus框架

整合了quartz

整合了线程池调用线程

新增登录拦截业务

整合了@Scheduled简易定时器任务

使用log4j2输出日志

整合了MyBatisPlus

整合了MyBatisPlus简易的代码生成器