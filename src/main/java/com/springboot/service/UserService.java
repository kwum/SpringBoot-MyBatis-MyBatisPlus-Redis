package com.springboot.service;

import javax.servlet.http.HttpSession;

/**
 * Description: 用户操作服务层接口
 * @author Kwum
 * @date 2017年4月21日 上午9:43:48
 * @version 1.0
 */

public interface UserService {

	/**
	 * 新增用户
	 * @param nick 昵称
	 * @return json数据
	 * @throws Exception
	 * @author kwum
	 */
    String addUser(String nick) throws Exception;
	
	/**
	 * 更新用户
	 * @param userId 用户id
	 * @param state 状态码：1正常、0删除、-99不修改，可不传，默认-99
	 * @param nick 昵称，可不传，默认空
	 * @return json数据
	 * @throws Exception
	 * @author kwum
	 */
    String updateUser(int userId, Integer state, String nick) throws Exception;

	/**
	 * 用户登录
	 * @param username 用户名
	 * @param password 密码
	 * @param code 验证码
	 * @param session
	 * @return
	 */
    String login(String username, String password, String code, HttpSession session);
}
