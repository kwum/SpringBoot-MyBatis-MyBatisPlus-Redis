package com.springboot.service.impl;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.service.ThreadService;

import java.util.concurrent.Future;


/**
 * Description: 线程操作服务层实现类
 * @author Kwum
 * @date 2017年10月14日 下午5:50:11
 * @version 1.0
 */

@Service
public class ThreadServiceImpl implements ThreadService{
	
    @Override
    @Async("defaultThreadPool")
    public void asyncThreadDemo(String threadName) throws Exception {
        final long time = 5000;
        System.out.println(threadName + " is working.");
        Thread.sleep(time);
        System.out.println(threadName + " is done.");
    }

    @Override
    @Async
    public Future<Boolean> doTask1() throws Exception {
        System.out.println("任务1开始...");
        long start = System.currentTimeMillis();
        Thread.sleep(5000);
        long end = System.currentTimeMillis();
        System.out.println("任务1完成，耗时：" + (end - start) + " 毫秒");
        return new AsyncResult<>(true);
    }

    @Override
    @Async
    public Future<Boolean> doTask2() throws Exception {
        System.out.println("任务2开始...");
        long start = System.currentTimeMillis();
        Thread.sleep(7000);
        long end = System.currentTimeMillis();
        System.out.println("任务2耗时：" + (end - start) + " 毫秒");
        return new AsyncResult<>(true);
    }

    @Override
    @Async
    public Future<Boolean> doTask3() throws Exception {
        System.out.println("任务3开始...");
        long start = System.currentTimeMillis();
        Thread.sleep(8000);
        long end = System.currentTimeMillis();
        System.out.println("任务3耗时：" + (end - start) + " 毫秒");
        return new AsyncResult<>(true);
    }
}
