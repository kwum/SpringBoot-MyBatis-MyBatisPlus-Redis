package com.springboot.service.impl;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import com.springboot.mapper.UserMapper;
import com.springboot.po.User;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.constant.SessionKey;
import com.springboot.dao.UserDao;
import com.springboot.service.UserService;
import com.springboot.utils.JsonFormatUtils;

/**
 * Description: 用户操作服务层实现类
 * @author Kwum
 * @date 2017年4月21日 上午9:44:21
 * @version 1.0
 */
@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserMapper userMapper;

	@Override
    @Transactional(rollbackFor = Exception.class)
	public String addUser(String nick) throws Exception {
		
		//插入用户数据
		User user = userDao.insertUser(nick);

		return JsonFormatUtils.success(user.getUserId());
	}

	@Override
    @Transactional(rollbackFor = Exception.class)
	public String updateUser(int userId, Integer state, String nick) throws Exception {
		
		//更新用户数据
		userDao.updateUser(userId, state, nick);
		
		return JsonFormatUtils.success();
	}

    @Override
    public String login(String username, String password, String code, HttpSession session) {
        
        //校验验证码
        String checkCode = (String) session.getAttribute(SessionKey.HTML_CAPTCHA);
        if(!StringUtils.equalsIgnoreCase(code, checkCode)){
            return JsonFormatUtils.error(1001);
        }
        
        //校验账号
        User userForSql = new User();
        userForSql.setUserNick(username);
        User user = userMapper.selectOne(userForSql);
        if(user == null){
            return JsonFormatUtils.error(1002);
        }
        
        //校验密码
        if(!StringUtils.equalsIgnoreCase(password, user.getState().toString())){
            return JsonFormatUtils.error(1003);
        }
        
        //登录成功，保存信息到session
        session.setAttribute(SessionKey.USER_ID, user.getUserId());
        
        return JsonFormatUtils.success();
    }
}
