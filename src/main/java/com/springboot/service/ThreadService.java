package com.springboot.service;

import java.util.concurrent.Future;

/**
 * Description: 线程操作服务层接口
 * @author Kwum
 * @date 2017年10月14日 下午5:49:28
 * @version 1.0
 */

public interface ThreadService {

    /**
     * 异步线程调用例子
     * @param threadName 线程名称
     * @return json数据
     * @throws Exception
     * @author kwum
     */
    void asyncThreadDemo(String threadName) throws Exception;

    /**
     * 异步任务测试
     * @throws Exception
     * @author kwum
     */
    Future<Boolean> doTask1() throws Exception;

    /**
     * 异步任务测试
     * @throws Exception
     * @author kwum
     */
    Future<Boolean> doTask2() throws Exception;

    /**
     * 异步任务测试
     * @throws Exception
     * @author kwum
     */
    Future<Boolean> doTask3() throws Exception;
}
