package com.springboot.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.springboot.po.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Kwum
 * @since 2018-06-21
 */
public interface UserMapper extends BaseMapper<User> {

}
