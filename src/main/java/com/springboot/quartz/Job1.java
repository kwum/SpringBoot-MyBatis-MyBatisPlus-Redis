package com.springboot.quartz;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.springboot.mapper.UserMapper;
import com.springboot.po.User;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Description: 定时任务1
 * @author Kwum
 * @date 2017年11月6日 上午10:45:55
 * @version 1.0
 */
@Component
public class Job1 implements Job{
    
    @Autowired
    private UserMapper userMapper;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("quartz1 run: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        try {
            System.out.println("quartz1 run: " + userMapper.selectPage(
                    new Page<User>(1, 10),
                    new EntityWrapper<User>().eq("state", "1")).toString());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
