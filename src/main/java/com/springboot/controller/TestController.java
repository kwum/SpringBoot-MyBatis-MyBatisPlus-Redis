package com.springboot.controller;

import com.springboot.pojo.KwumProperties;
import com.springboot.service.ThreadService;
import com.springboot.utils.JsonFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Future;

/**
 * Description: 测试控制器
 * @author Kwum
 * @date 2017年4月19日 下午5:19:01
 * @version 1.0
 */
@RestController
@RequestMapping("test")
public class TestController {

	/**
	 * 读取yml文件中的自定义参数
	 */
	@Autowired
	private KwumProperties kwumProperties;
	@Autowired
	private ThreadService threadService;
	
	@RequestMapping("hello")
	public String hello() {
		
		//获取自定义参数
//		System.out.println("Hello Spring Boot! " + kwumProperties.getHeight());
		
		//获取user
//		System.out.println(userMapper.selectByExample(null).get(0).getUserNick());
		
		//模拟异常
//		System.out.println(1/0);
		
		//hello world!
		return "Hello Spring Boot!";
	}
	
	@RequestMapping("helloworld")
    public String helloworld() {
        
        //hello world!
        return JsonFormatUtils.success("Hello World, Spring Boot!");
    }
	
	/**
	 * 测试异步线程
	 * @return
	 * @throws Exception
	 * @author kwum
	 */
	@RequestMapping(value="asynThread")
    public String threadTest() throws Exception{
		long start = System.currentTimeMillis();

		Future<Boolean> a = threadService.doTask1();
		Future<Boolean> b = threadService.doTask2();
		Future<Boolean> c = threadService.doTask3();

		while (!a.isDone() || !b.isDone() || !c.isDone()){
			if (a.isDone() && b.isDone() && c.isDone()){
				break;
			}
		}

		long end = System.currentTimeMillis();
		System.out.println("任务全部完成，总耗时：" + (end - start) + " 毫秒");

		return JsonFormatUtils.success("Hello World, asyn thread!");
	}
}
