package com.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Description: springboot项目启动器（需与controller放在同一个包中）
 * @author Kwum
 * @date 2017年4月19日 下午5:19:01
 * @version 1.0
 */
@SpringBootApplication
//扫描所需要的包
@ComponentScan(basePackages = {
		"com.springboot.controller",
        "com.springboot.pojo",
		"com.springboot.service",
		"com.springboot.dao",
		"com.springboot.filter",
		"com.springboot.handle",
		"com.springboot.config",
//		"com.springboot.quartz",
//		"com.springboot.timer"
		})
//扫描mybatis mapper 包路径
@MapperScan("com.springboot.mapper")
//开启定时任务
@EnableScheduling
public class RunApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(RunApplication.class, args);
	}

	/**
	 * 以war包形式发布时，需要继承SpringBootServletInitializer复写其中的configure方法
	 * @param application
	 * @return
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(this);
	}
}
