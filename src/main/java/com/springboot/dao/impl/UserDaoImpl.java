package com.springboot.dao.impl;

import javax.annotation.Resource;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.springboot.dao.UserDao;
import com.springboot.mapper.UserMapper;
import com.springboot.po.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

/**
 * Description: 用户dao层实现类
 * @author Kwum
 * @date 2017年4月21日 下午3:32:06
 * @version 1.0
 */
@Repository
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private UserMapper userMapper;

	/**
     * @Cacheable 应用到读取数据的方法上，先从缓存中读取，如果没有再从DB获取数据，然后把数据添加到缓存中
     * unless 表示条件表达式成立的话不放入缓存
     */
	@Override
	@Cacheable(cacheNames = "countUserByState")
	public int countUserByState(Integer state) throws Exception {
		return userMapper.selectCount(new EntityWrapper<User>().eq("state", state));
	}

	/**
     * @CachePut 应用到写数据的方法上，如新增/修改的方法，调用方法时会自动把相应的数据放入缓存
     * @CacheEvict 应用到删除数据的方法上，调用方法时会从缓存中删除对应key的数据
     */
    @Override
    @CachePut(cacheNames = "user")
	public User insertUser(String nick) throws Exception {
		User user = new User();
		user.setUserNick(nick);
		user.setState(1);
		userMapper.insert(user);
		System.out.println("userId....................." + user.getUserId());
        return user;
	}

	@Override
	@CachePut(cacheNames = "user")
	public User updateUser(int userId, Integer state, String nick) throws Exception {
		User user = new User();
		user.setUserId(userId);
		if(state != null){
			user.setState(state);
		}
		if(nick != null){
			user.setUserNick(nick);
		}
		userMapper.updateAllColumnById(user);
        return user;
	}
}
